//
//  BSLSuccessMsg.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLSuccessMsg : NSObject

@property (nonatomic) BOOL isSuccess;

@end
