//
//  BSLRouteManager.m
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLRouteManager.h"
#import "RKObjectMapping.h"
#import "BSLError.h"

@implementation BSLRouteManager

    @synthesize responseDescriptorDict;
    @synthesize requestDescriptorArr;
    @synthesize routeSet;

    -(instancetype) initWithLoadDatas{
        self = [super init];
        return self;
    }

    -(void) reloadDatas
    {
        requestDescriptorArr = [[NSMutableArray alloc] init];
        responseDescriptorDict = [[NSMutableDictionary alloc] init];
        routeSet = [[NSMutableArray alloc] init];
        
        [self loadErrorResponseDescriptor];
    }

    -(void) loadErrorResponseDescriptor
    {
        RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[BSLError class]];
        [errorMapping addAttributeMappingsFromDictionary:@{
                                                           @"errorCode":@"errorCode",
                                                           @"errorMsg":@"errorMsg",
                                                           @"errorURL":@"errorURL"
                                                           }];
        NSIndexSet *errorCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassServerError);
        RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:errorMapping method:RKRequestMethodAny pathPattern:nil keyPath:@"error" statusCodes:errorCodes];
        [self setErrRespDesc:errorDescriptor];
    }



@end
