//
//  BSLData.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-6-3.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLData : NSObject

    @property (strong,nonatomic) NSString *data;

@end
