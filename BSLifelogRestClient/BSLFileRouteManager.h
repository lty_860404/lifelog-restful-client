//
//  BSLFileRouteManager.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-6-9.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLRouteManager.h"


#define ROUTE_FILE_GET_ID "fileGet"
#define ROUTE_FILE_GET_PATH "/files/:fileKey"

@interface BSLFileRouteManager : BSLRouteManager

    

@end
