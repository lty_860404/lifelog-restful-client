//
//  BSLRouteManager.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RKResponseDescriptor.h"
#import "RKRequestDescriptor.h"
#import "RKRoute.h"

#define ERROR_MSG_NAME "error500"

@interface BSLRouteManager : NSObject

    @property (strong,nonatomic) NSMutableArray *routeSet;
    @property (strong,nonatomic) NSMutableArray *requestDescriptorArr;
    @property (strong,nonatomic) NSMutableDictionary *responseDescriptorDict;

    @property (strong,nonatomic) RKResponseDescriptor* errRespDesc;

    -(instancetype) initWithLoadDatas;
    -(void) reloadDatas;

@end
