//
//  BSLRestKit.m
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLRestClient.h"


@implementation BSLRestClient

    @synthesize manager;

    -(instancetype) initWithBaseURL:(NSURL *)baseURL
    {
        self = [super init];
        if(self!=nil){
            manager = [RKObjectManager managerWithBaseURL:baseURL];
            [self setDefaultRequestSerializationMIMEType];
            [self setDefaultAcceptHeaderWithMIMEType];
        }
        return self;
    }

    -(void) loadRoutes:(BSLRouteManager *)routeManager
    {
        [manager addRequestDescriptorsFromArray:routeManager.requestDescriptorArr];
        [manager.router.routeSet addRoutes:routeManager.routeSet];
        [self setResponseDescriptorDict:[routeManager responseDescriptorDict]];
        [self setErrRespDesc:[routeManager errRespDesc]];
        [manager addResponseDescriptor:self.errRespDesc];
    }

    -(void) addRequestDescriptor:(RKRequestDescriptor *)requestDescriptor
    {
        [manager addRequestDescriptor:requestDescriptor];
    }

    -(void) addRequestDescriptorFromArray:(NSArray *)arr
    {
        [manager addRequestDescriptorsFromArray:arr];
    }

    -(void) addRoute:(RKRoute *)route
    {
        [manager.router.routeSet addRoute:route];
    }

    -(void) waitUntilAllAFNetworkRestFinished
    {
        while(manager.HTTPClient.operationQueue.operationCount>0){
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate: [NSDate dateWithTimeIntervalSinceNow:0.05]];
        }
    }

    -(void) getDataByPath:(NSString *)path
           parameters:(NSDictionary *)parameters
              success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
    {
        [manager.HTTPClient getPath:path
                         parameters:parameters
                            success:success
                            failure:failure];
        [self waitUntilAllAFNetworkRestFinished];
        [self setDefaultRequestSerializationMIMEType];
        [self setDefaultAcceptHeaderWithMIMEType];
    }


    -(void)postObject:(id)object
             identify:(NSString *)identify
                 path:(NSString *)path
           parameters:(NSDictionary *)parameters
              success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
              failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure
    {
        RKResponseDescriptor* respDesc = [self.responseDescriptorDict objectForKey:identify];
        [manager addResponseDescriptor:respDesc];
        [manager postObject:object
                       path:path
                 parameters:parameters
                    success:success
                    failure:failure];
        [self setDefaultRequestSerializationMIMEType];
        [self setDefaultAcceptHeaderWithMIMEType];
        [self removeResponseDescriptorAfterAllOperationsAreFinished:respDesc];
    }

    -(void)postObject:(id)object
 relationShipIdentify:(NSString *)identify
           parameters:(NSDictionary *)parameters
              success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
              failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure
    {
        NSURL *url = [manager.router URLForRelationship:identify
                                               ofObject:object
                                                 method:RKRequestMethodPOST];
        RKResponseDescriptor* respDesc = [self.responseDescriptorDict objectForKey:identify];
        [manager addResponseDescriptor:respDesc];
        [manager postObject:object
                   path:[url path]
             parameters:parameters
                success:success
                failure:failure];
        [self setDefaultRequestSerializationMIMEType];
        [self setDefaultAcceptHeaderWithMIMEType];
        [self removeResponseDescriptorAfterAllOperationsAreFinished:respDesc];
    }

-(void) getObject:(id)object
         identify:(NSString *)identify
             path:(NSString *)path
       parameters:(NSDictionary *)parameters
          success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
          failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;
    {
        RKResponseDescriptor* respDesc = [self.responseDescriptorDict objectForKey:identify];
        [manager addResponseDescriptor:respDesc];
        [manager getObject:object
                      path:path
                parameters:parameters
                   success:success
                   failure:failure];
        [self setDefaultRequestSerializationMIMEType];
        [self setDefaultAcceptHeaderWithMIMEType];
        [self removeResponseDescriptorAfterAllOperationsAreFinished:respDesc];
    }

-(void) getObject:(id)object
    routeIdentify:(NSString *)identify
       parameters:(NSDictionary *)parameters
          success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
          failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;
    {
        RKResponseDescriptor* respDesc = [self.responseDescriptorDict objectForKey:identify];
        [manager addResponseDescriptor:respDesc];
        [manager getObjectsAtPathForRouteNamed:identify
                                        object:object
                                    parameters:parameters
                                       success:success
                                       failure:failure];
        [self setDefaultRequestSerializationMIMEType];
        [self setDefaultAcceptHeaderWithMIMEType];
        [self removeResponseDescriptorAfterAllOperationsAreFinished:respDesc];
    }


-(void) getObject:(id)object
relationshipIdentify:(NSString *)identify
       parameters:(NSDictionary *)parameters
          success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
          failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;
    {
        RKResponseDescriptor* respDesc = [self.responseDescriptorDict objectForKey:identify];
        [manager addResponseDescriptor:respDesc];
        [manager getObjectsAtPathForRelationship:identify
                                        ofObject:object
                                      parameters:parameters
                                         success:success
                                         failure:failure];
        [self setDefaultRequestSerializationMIMEType];
        [self setDefaultAcceptHeaderWithMIMEType];
        [self removeResponseDescriptorAfterAllOperationsAreFinished:respDesc];
    }

-(void) deleteObject:(id)object
        relationship:(NSString *)identify
                path:(NSString *)path
          parameters:(NSDictionary *)parameters
             success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
             failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure{
        RKResponseDescriptor* respDesc = [self.responseDescriptorDict objectForKey:identify];
        [manager addResponseDescriptor:respDesc];
        NSURL *url = [manager.router URLForRelationship:identify
                                  ofObject:object
                                    method:RKRequestMethodDELETE];
        [manager deleteObject:object
                         path:[url path]
                   parameters:parameters
                      success:success
                      failure:failure];
        [self setDefaultRequestSerializationMIMEType];
        [self setDefaultAcceptHeaderWithMIMEType];
        [self removeResponseDescriptorAfterAllOperationsAreFinished:respDesc];
    }

    -(void) uploadImage:(UIImage *)image
                   name:(NSString *)name
               filename:(NSString *)filename
                 object:(id)object
           relationShip:(NSString *)identify
             parameters:(NSDictionary *)parameters
                success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
                failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure{
        RKResponseDescriptor* respDesc = [self.responseDescriptorDict objectForKey:identify];
        [manager addResponseDescriptor:respDesc];
        NSURL *url = [manager.router URLForRelationship:identify
                                               ofObject:object
                                                 method:RKRequestMethodPOST];
        NSMutableURLRequest *request = [manager multipartFormRequestWithObject:object
                                                                    method:RKRequestMethodPOST
                                                                      path:[url path]
                                                                parameters:parameters
                                                 constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                     if(image==nil){
                                                         NSLog(@"image is not exist!");
                                                     }
                                                     [formData appendPartWithFileData:UIImagePNGRepresentation(image)
                                                                                 name:name
                                                                             fileName:filename
                                                                             mimeType:@"image/PNG"];
                                                 }];
    
        RKObjectRequestOperation *operation = [manager objectRequestOperationWithRequest:request
                                                                                success:success
                                                                                failure:failure];
        [manager enqueueObjectRequestOperation:operation];
        [self removeResponseDescriptorAfterAllOperationsAreFinished:respDesc];
        [self setDefaultRequestSerializationMIMEType];
        [self setDefaultAcceptHeaderWithMIMEType];
    }



    -(void) removeResponseDescriptorAfterAllOperationsAreFinished:(RKResponseDescriptor *)responseDesc
    {
        [self doActionAfterAllRestFinished:^{
            [manager removeResponseDescriptor:responseDesc];
        }];
    }

    -(void) waitUntilAllRestFinished{
        while(manager.operationQueue.operationCount>0){
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate: [NSDate dateWithTimeIntervalSinceNow:0.05]];
        }
    }

    -(void) doActionAfterAllRestFinished:(void (^)(void))action{
        [self waitUntilAllRestFinished];
        action();
    }

    -(void) setDefaultRequestSerializationMIMEType{
        if(![RKMIMETypeFormURLEncoded isEqualToString:[self requestSerializationMIMEType]]){
            [self setRequestSerializationMIMEType:RKMIMETypeFormURLEncoded];
        }
    }

    -(void) setRequestSerializationMIMEType:(NSString *)MIMEType
    {
        [manager setRequestSerializationMIMEType:MIMEType];
    }

    -(NSString *) requestSerializationMIMEType{
        return [manager requestSerializationMIMEType];
    }

    -(void) setDefaultAcceptHeaderWithMIMEType{
        if(![RKMIMETypeJSON isEqualToString:[self acceptHeaderWithMIMEType]]){
            [self setAcceptHeaderWithMIMEType:RKMIMETypeJSON];
        }
    }

    -(void) setAcceptHeaderWithMIMEType:(NSString *)MIMEType{
        [manager setAcceptHeaderWithMIMEType:MIMEType];
    }

    -(NSString *) acceptHeaderWithMIMEType{
        return [[manager defaultHeaders] objectForKey:@"Accept"];
    }

@end
