//
//  BSLRestKit.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RKObjectManager.h"
#import "RKRequestDescriptor.h"
#import "RKResponseDescriptor.h"
#import "BSLRouteManager.h"
#import "RKMIMETypeSerialization.h"

@interface BSLRestClient : NSObject

@property BSLRouteManager *routeManager;
@property (strong,nonatomic) RKObjectManager* manager;

@property (strong,nonatomic) NSMutableDictionary *responseDescriptorDict;
@property (strong,nonatomic) RKResponseDescriptor* errRespDesc;

-(instancetype) initWithBaseURL:(NSURL *)baseURL;

-(void) addRequestDescriptorFromArray:(NSArray *)arr;
-(void) addRequestDescriptor:(RKRequestDescriptor *)requestDescriptor;

-(void) addRoute:(RKRoute *)route;

-(void) loadRoutes:(BSLRouteManager *)routeManager;

-(void)postObject:(id)object
         identify:(NSString *)identify
             path:(NSString *)path
       parameters:(NSDictionary *)parameters
          success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
          failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;

-(void)postObject:(id)object
relationShipIdentify:(NSString *)identify
       parameters:(NSDictionary *)parameters
          success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
          failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;

-(void) getObject:(id)object
         identify:(NSString *)identify
             path:(NSString *)path
       parameters:(NSDictionary *)parameters
          success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
          failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;

-(void) getObject:(id)object
    routeIdentify:(NSString *)identify
       parameters:(NSDictionary *)parameters
          success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
          failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;

-(void) getObject:(id)object
relationshipIdentify:(NSString *)identify
       parameters:(NSDictionary *)parameters
          success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
          failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;

-(void) deleteObject:(id)object
        relationship:(NSString *)identify
                path:(NSString *)path
          parameters:(NSDictionary *)parameters
             success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
             failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;

-(void) uploadImage:(UIImage *)image
               name:(NSString *)name
           filename:(NSString *)filename
             object:(id)object
       relationShip:(NSString *)identify
         parameters:(NSDictionary *)parameters
            success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
            failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;

-(void) getDataByPath:(NSString *)path
           parameters:(NSDictionary *)parameters
              success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void) waitUntilAllRestFinished;
-(void) removeResponseDescriptorAfterAllOperationsAreFinished:(RKResponseDescriptor *)responseDesc;
-(void) doActionAfterAllRestFinished:(void (^)(void))action;

-(void) setDefaultRequestSerializationMIMEType;
-(void) setRequestSerializationMIMEType:(NSString *)RKMIMEType;
-(NSString *) requestSerializationMIMEType;

-(void) setDefaultAcceptHeaderWithMIMEType;
-(void) setAcceptHeaderWithMIMEType:(NSString *)MIMEType;
-(NSString *) acceptHeaderWithMIMEType;

@end
