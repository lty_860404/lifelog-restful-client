//
//  BSLUser.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLUser : NSObject

@property (nonatomic) long id;
@property (copy,nonatomic) NSString *username;
@property (copy,nonatomic) NSString *password;
@property (copy,nonatomic) NSString *name;
@property (nonatomic) long familyId;
@property (copy,nonatomic) NSString *phone;
@property (copy,nonatomic) NSString *gender;
@property (copy,nonatomic) NSString *signature;
@property (copy,nonatomic) NSString *avatarFilePath;
@property (copy,nonatomic) NSString *langCode;
@property (copy,nonatomic) NSDate *registerTime;

@end
