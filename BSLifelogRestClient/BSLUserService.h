//
//  BSLUserService.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSLService.h"
#import "BSLRestClient.h"
#import "BSLSuccessMsg.h"
#import "BSLError.h"
#import "BSLUser.h"
#import "BSLUserAddParams.h"
#import "BSLUserRouteManager.h"
#import "BSLData.h"
#import "BSLUserUpdateParams.h"

@interface BSLUserService : BSLService

    +(instancetype) getInstance;

    //if success return BSLSuccessMsg else return BSLError
    -(void) verify:(NSString *)phone
           success:(void (^)(BSLSuccessMsg *successMsg))success
           failure:(void (^)(BSLError *err))failure;

    -(void) getUserInfoWithId:(long)userId
                      success:(void (^)(BSLUser *user))success
                      failure:(void (^)(BSLError *err))failure;

    -(void) getUsersInfoSuccess:(void (^)(NSArray *usersInfoArr))success
                        failure:(void (^)(BSLError *err))failure;

    -(void) addUser:(BSLUserAddParams *)user
            success:(void (^)(BSLSuccessMsg *successMsg))success
            failure:(void (^)(BSLError *err))failure;

    -(void) updateUserInfo:(BSLUserUpdateParams *)userUpdateParam
                   success:(void (^)(BSLSuccessMsg *successMsg))success
                   failure:(void (^)(BSLError *err))failure;

    -(void) delUserWithId:(long)userId
                  success:(void (^)(BSLSuccessMsg *successMsg))success
                  failure:(void (^)(BSLError *err))failure;

    -(void) changeAvator:(UIImage*)avator
                filename:(NSString *)filename
                  userId:(long)userId
                 success:(void (^)(BSLData *data))success
                 failure:(void (^)(BSLError *err))failure;

@end
