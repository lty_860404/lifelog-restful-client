//
//  BSLError.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLError : NSObject

@property (copy,nonatomic) NSString *errorCode;
@property (copy,nonatomic) NSString *errorMsg;
@property (copy,nonatomic) NSString *errorURL;

@end
