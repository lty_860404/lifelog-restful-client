//
//  BSLUserAddInfo.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-29.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLUserAddParams : NSObject

    @property (copy,nonatomic) NSString *verifyCode;
    @property (copy,nonatomic) NSString *username;
    @property (copy,nonatomic) NSString *password;
    @property (copy,nonatomic) NSString *name;
    @property (copy,nonatomic) NSString *phone;
    @property (copy,nonatomic) NSString *familyName;

@end
