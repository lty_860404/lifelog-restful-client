//
//  BSLSystemConfig.m
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLSystemConfig.h"

@implementation BSLSystemConfig

static NSURL *baseURL;

+(NSURL *) baseURL{
    return baseURL;
}

+(void) setBaseURL:(NSURL *)url{
    baseURL = url;
}

@end
