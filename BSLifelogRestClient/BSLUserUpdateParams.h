//
//  BSLUserUpdateParams.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-6-6.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLUserUpdateParams : NSObject

@property (nonatomic) long id;
@property (strong,nonatomic) NSString* username;
@property (strong,nonatomic) NSString* password;
@property (strong,nonatomic) NSString* name;
@property (strong,nonatomic) NSString* familyId;
@property (strong,nonatomic) NSString* phone;
@property (strong,nonatomic) NSString* gender;
@property (strong,nonatomic) NSString* signature;

@end
