//
//  BSLFileKey.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-6-9.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLFileKey : NSObject

@property (strong,nonatomic) NSString *fileKey;

@end
