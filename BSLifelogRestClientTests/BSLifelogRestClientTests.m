//
//  BSLifelogRestClientTests.m
//  BSLifelogRestClientTests
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RKObjectManager.h"
#import "RKLog.h"
#import "BSLUserService.h"
#import "BSLSystemConfig.h"
#import "RKMappingTest.h"
#import "BSLFileService.h"


@interface BSLifelogRestClientTests : XCTestCase
    @property (strong,nonatomic) BSLUserService *userService;
    @property (strong,nonatomic) BSLFileService *fileService;
@end

@implementation BSLifelogRestClientTests

@synthesize userService;
@synthesize fileService;

- (void)setUp
{
    [super setUp];
    // Replace the default object manager factory
    [BSLSystemConfig setBaseURL:[[NSURL alloc] initWithString:@"http://192.168.1.102:8080"]];
    
    userService = [BSLUserService getInstance];
    fileService = [BSLFileService getInstance];
    
    // Log all HTTP traffic with request and response bodies
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    // Log debugging info about Core Data
    RKLogConfigureByName("RestKit/CoreData", RKLogLevelDebug);
    // Put setup code here. This method is called before the invocation of each test method in the class.

}

- (void)tearDown
{
    [super tearDown];
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

BOOL flag;

-(void) testFileGetRest{
    [fileService getFile:@"4deedac8-248a-4bf9-a2b0-5868ef811a14_avatar"
                 success:^(NSData *data) {
                     
                 } failure:^(NSError *err) {
                     
                 }];
}

-(void) testUserVerify
{
    [userService verify:@"+86 13123456789"
                success:^(BSLSuccessMsg *successMsg) {
                    flag = successMsg.isSuccess;
                    NSLog(@"isSuccess:%@",successMsg.isSuccess?@"Yes":@"No");
                }
                failure:^(BSLError *err) {
                    NSLog(@"errCode:%@",err.errorCode);
                    NSLog(@"errURL:%@",err.errorURL);
                    NSLog(@"errMsg:%@",err.errorMsg);
                }];
    NSLog(@"isSuccess:%@",flag?@"YES":@"NO");
}

-(void) testUpdateUser
{
    BSLUserUpdateParams* userUpdateParams = [[BSLUserUpdateParams alloc] init];
    [userUpdateParams setId:1];
    [userUpdateParams setUsername:@"lty860404"];
    [userUpdateParams setPassword:@"7darksky"];
    [userUpdateParams setPhone:@"13020117744"];
    [userUpdateParams setGender:@"M"];
    [userUpdateParams setSignature:@"lalalilalilali......"];
    [userService updateUserInfo:userUpdateParams
                        success:^(BSLSuccessMsg *successMsg) {
                            NSLog(@"user update result:%@",successMsg.isSuccess?@"Yes":@"No");
                        } failure:^(BSLError *err) {                            NSLog(@"errCode:%@",err.errorCode);
                            NSLog(@"errURL:%@",err.errorURL);
                            NSLog(@"errMsg:%@",err.errorMsg);
                        }];
}

-(void) testUserInfo
{
    [userService getUserInfoWithId:1
                           success:^(BSLUser *user) {
                               NSLog(@"user Info name:%@",user.name);
                               NSLog(@"user Info phone:%@",user.phone);
                           }
                           failure:^(BSLError *err) {
                               NSLog(@"errCode:%@",err.errorCode);
                               NSLog(@"errURL:%@",err.errorURL);
                               NSLog(@"errMsg:%@",err.errorMsg);
                           }];
    //这里有问题，大于100可以8
    [userService getUserInfoWithId:100
                           success:^(BSLUser *user) {
                               NSLog(@"user Info name:%@",user.name);
                               NSLog(@"user Info phone:%@",user.phone);
                           }
                           failure:^(BSLError *err) {
                               NSLog(@"errCode:%@",err.errorCode);
                               NSLog(@"errURL:%@",err.errorURL);
                               NSLog(@"errMsg:%@",err.errorMsg);
                           }];

}

-(void) testUsersInfo
{
    [userService getUsersInfoSuccess:^(NSArray *usersInfoArr) {
        for(int i=0;i<usersInfoArr.count;i++){
            BSLUser* user = [usersInfoArr objectAtIndex:i];
            NSLog(@"user Id:%ld",user.id);
            NSLog(@"username:%@",user.username);
            NSLog(@"user phone:%@",user.phone);
        }
    } failure:^(BSLError *err) {
        NSLog(@"errCode:%@",err.errorCode);
        NSLog(@"errURL:%@",err.errorURL);
        NSLog(@"errMsg:%@",err.errorMsg);
    }];
}

-(void) testUserRemoveAndAdd
{
    [userService delUserWithId:4
                       success:^(BSLSuccessMsg *successMsg) {
                           flag = successMsg.isSuccess;
                           NSLog(@"isSuccess:%@",successMsg.isSuccess?@"Yes":@"No");
                       } failure:^(BSLError *err) {
                           NSLog(@"errCode:%@",err.errorCode);
                           NSLog(@"errURL:%@",err.errorURL);
                           NSLog(@"errMsg:%@",err.errorMsg);
                       }]; 
    BSLUserAddParams *user = [[BSLUserAddParams alloc] init];
    [user setVerifyCode:@"tc26"];
    [user setUsername:@"test0"];
    [user setPassword:@"test0"];
    [user setName:@"test0"];
    [user setPhone:@"13012345678"];
    [user setFamilyName:@"test_HOME"];
    [userService addUser:user
                 success:^(BSLSuccessMsg *successMsg) {
                     flag = successMsg.isSuccess;
                     NSLog(@"isSuccess:%@",successMsg.isSuccess?@"Yes":@"No");
                 } failure:^(BSLError *err) {
                     NSLog(@"errCode:%@",err.errorCode);
                     NSLog(@"errURL:%@",err.errorURL);
                     NSLog(@"errMsg:%@",err.errorMsg);
                 }];
}


-(void) testChangeAvatar
{
    NSURL *url = [NSURL URLWithString:@"http://pic1.nipic.com/2008-09-05/20089572025383_2.jpg"];
    UIImage *avatar = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    [userService changeAvator:avatar
                     filename:@"20089572025383_2.jpg"
                       userId:1
                      success:^(BSLData *data) {
                          NSLog(@"data:%@",data.data);
                      } failure:^(BSLError *err) {
                          NSLog(@"errCode:%@",err.errorCode);
                          NSLog(@"errURL:%@",err.errorURL);
                          NSLog(@"errMsg:%@",err.errorMsg);
                      }];
}

-(void) testBSLErrorMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[BSLError class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                    @"errorCode":@"errorCode",
                                                    @"errorMsg":@"errorMsg",
                                                    @"errorURL":@"errorURL"
                                                }];
    
    BSLError* error = [[BSLError alloc] init];
    [error setErrorCode:@"12234"];
    [error setErrorURL:@"http://dem2local.dodopipe.com:8080/lifelog2/"];
    [error setErrorMsg:@"this is a test!!!!!"];
    RKMappingTest *mappingTest = [[RKMappingTest alloc] initWithMapping:mapping sourceObject:error destinationObject:nil];
    
    RKPropertyMappingTestExpectation* errCodeExpectation = [RKPropertyMappingTestExpectation expectationWithSourceKeyPath:@"errorCode" destinationKeyPath:@"errorCode" value:@"12234"];
    RKPropertyMappingTestExpectation* errURLExpectation = [RKPropertyMappingTestExpectation expectationWithSourceKeyPath:@"errorURL" destinationKeyPath:@"errorURL" value:@"http://dem2local.dodopipe.com:8080/lifelog2/"];
    RKPropertyMappingTestExpectation* errMsgExpectation = [RKPropertyMappingTestExpectation expectationWithSourceKeyPath:@"errorMsg" destinationKeyPath:@"errorMsg" value:@"this is a test!!!!!"];
    [mappingTest addExpectation:errCodeExpectation];
    [mappingTest addExpectation:errURLExpectation];
    [mappingTest addExpectation:errMsgExpectation];
    [mappingTest performMapping];
    [mappingTest verify];
    NSLog(@"%@",[mappingTest evaluate]?@"test is pass":@"test failed");
}

-(void) testBSLSuccessMsgMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[BSLSuccessMsg class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                          @"success":@"isSuccess"
                                                          }];
    
    BSLSuccessMsg* successMsg = [[BSLSuccessMsg alloc] init];
    [successMsg setIsSuccess:YES];
    
    RKMappingTest *mappingTest = [[RKMappingTest alloc] initWithMapping:mapping sourceObject:successMsg destinationObject:nil];
    
    RKPropertyMappingTestExpectation* successFlag = [RKPropertyMappingTestExpectation expectationWithSourceKeyPath:@"success" destinationKeyPath:@"isSuccess"];
    [mappingTest addExpectation:successFlag];
    
    [mappingTest performMapping];
    [mappingTest verify];
    NSLog(@"%@",[mappingTest evaluate]?@"test is pass":@"test failed");
}

@end
